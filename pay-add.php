<?php
require 'PayModel.php';
# 抓取 pay.php 中 item, dollars 輸入的資料
# 抓取日期
$item    = $_POST['new-pay-item'];
$dollars = $_POST['new-pay-dollars'];
$date    = $_POST['new-pay-date'];

$pay = [
  'item' => $item,
  'dollars' => $dollars,
  'date' => $date,
];
# 新增資料到資料庫
$success = create($pay);

if($success) {
  #   跳回 pay.php
  $host  = $_SERVER['HTTP_HOST'] . '/tom-pay';
  $url = $host . '/' . 'pay.php?date=' . $date; header("Location: http://$url");
  exit;
} else {
  throw new \Exception("it can't create new pay !", 1);
}

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Tom Pay - What do I buy ?</title>
    <link href="application.css" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <h2>
      <a href="pay.php?date=<?php echo $today; ?>">Pay of Today</a>
      Total of <?php echo $calendarTitle; ?>:
      <span id="total_of_month"><?php echo $total_of_month; ?></span>
      <a href="index.php?date=<?php echo $lastMonthYear; ?>">« <?php echo $lastMonthYearTitle; ?></a>
      | <a href="index.php?date=<?php echo $nextMonthYear; ?>"><?php echo $nextMonthYearTitle; ?> »</a>
    </h2>

    <table id="calendar">
      <tr>
<?php
# 顯示月曆
## 顯示星期
for($col = 0; $col < count($calendar[0]); $col++) { ?>
    <td><?php echo $calendar[0][$col]; ?></td>
  <?php
  # 顯示最右邊每星期小計的標題
  if($col == 6) {
  ?>
    <td><?php echo $week_sum[0]['sum']; ?></td>
<?php
  }
}
?>
</tr>
<?php
# 顯示日期和小計
for($row = 1; $row < count($calendar); $row++) { ?>
  <tr>
  <?php
  for($col = 0; $col < count($calendar[$row]); $col++) {
    $date = $calendar[$row][$col]['date'];
  ?>
  <td>
    <ul>
      <a href='pay.php?date=<?php echo $date; ?>'>
	<li>
          <?php echo showOnlyDay($date); ?>
	</li>
	<li>
	  <?php echo showSum($calendar[$row][$col]['sum']); ?>
	</li>
      </a>
    </ul>
  </td>
  <?php
    # 顯示最右邊每星期小計
    if($col == 6) {
    ?>
  <td><?php echo showOnlyDay($calendar[$row][0]['date']) . ' ~ ' . showOnlyDay($calendar[$row][6]['date']); ?>
  <br><?php echo $week_sum[$row]['sum']; ?>
  </td>
  <?php
    }
  } ?>
  </tr>
<?php
}
?>
</table>
</body>
</html>

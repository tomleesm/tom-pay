<?php
require 'PayModel.php';

$id = $_GET['id'];
$date = $_GET['date'];

# 刪除資料
$success = remove($id);

if($success) {
  # 跳回 pay.php
  $host  = $_SERVER['HTTP_HOST'] . '/tom-pay';
  $url = $host . '/' . 'pay.php?date=' . $date;

  header("Location: http://$url");
} else {
  throw new \Exception("it can't remove pay!", 1);
}

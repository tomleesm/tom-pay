<?php
require 'PayModel.php';
$date = get_date();
$title = sprintf('%s - Tom Pay', showFullDate($date));
$calendarUrl = sprintf('index.php?date=%s', substr($date, 0, 7));
$calendarTitle = sprintf('Calendar - %s', showLinkCalendar($date));
$fullDate = showFullDate($date);

// 抓取明細
$list = getAll($date);
# 計算小計
$sum = 0;
foreach ($list as $row) {
  $sum += $row['dollars'];
}

include 'pay-view.php';

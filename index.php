<?php
require 'PayModel.php';

$calendar = getCalendar();
# 計算這個月支出總計，儲存到變數 $total_of_month
$total_of_month = total_of_month();

# 最右邊顯示每星期小計
# 計算每星期小計
$week_sum = getWeekSum();

$calendarTitle = showCalendarTitle();
$today = getToday();
$lastMonthYear = lastMonthYear();
$nextMonthYear = nextMonthYear();
$lastMonthYearTitle = lastMonthYearTitle();
$nextMonthYearTitle = nextMonthYearTitle();
include('index-view.php');

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>

  <meta content="text/html; charset=UTF8" http-equiv="content-type">
  <title><?php echo $title; ?></title>
  <link href="application.css" media="all" rel="stylesheet" type="text/css" />

</head><body>
<h2><a href="<?php echo $calendarUrl; ?>"><?php echo $calendarTitle; ?></a></h2>

  <form method="post" action="pay-add.php" name="new-pay">
  <label>What do I buy ?</label>
    <p id="new-pay-item"><input name="new-pay-item" autofocus /></p>
  <label>How much ?</label>
    <p id="new-pay-dollars"><input name="new-pay-dollars" /></p>
    <input name="new-pay-date" type="hidden" value="<?php echo $date; ?>" />
    <input value="submit" type="submit" />
  </form>

<h3><?php echo $fullDate; ?></h3>

<table id="pay-list">

  <tbody>
  <?php
    foreach($list as $i):
      ?>
      <tr>
        <td><?php echo $i['item']; ?></td>
        <td><?php echo $i['dollars']; ?></td>
      	<td>
          <a href="#" onclick="del_confirm('<?php echo $i['item']; ?>', <?php echo $i['id']; ?>)">delete</a>
        </td>
      </tr>
    <?php endforeach; ?>
      <tr id="sum">
        <td>Sum<br>
        <td><?php echo $sum; ?></td>
        <td><br>
    </tr>
  </tbody>
</table>

</body>
<script type="text/javascript">
function del_confirm(item, id) {
  var warming_msg = 'Delete ' + item + ' ?';
  var result = confirm(warming_msg);

  if(result == true) {
    location.href="pay-del.php?id=" + id + "&date=<?php echo $date; ?>";
  }
  else {}
}
</script>
</html>

<?php
function showFullDate($date) {
  date_default_timezone_set('Asia/Taipei');
  # 抓取年月日
  $year  = substr($date, 0, 4);
  $month = substr($date, 5, 2);
  $day   = substr($date, 8, 2);

  # 星期
  $day_of_week = date("l", mktime(0, 0, 0, $month, $day, $year));
  # December
  $fullMonth = date("F", mktime(0, 0, 0, $month, $day, $year));
  # 日，沒有0
  $showDay = sprintf("%d", $day);

  # Monday, December 1 2014
  return "$day_of_week, $fullMonth $showDay $year";
}
function showLinkCalendar($date) {
  date_default_timezone_set('Asia/Taipei');
  $year  = substr($date, 0, 4);
  $month = substr($date, 5, 2);
  $fullMonth = date("F", mktime(0, 0, 0, $month, 1, $year));

  return "$fullMonth $year";
}
function getAll($date) {
  try {
    $pdo = new PDO(
      sprintf('mysql:host=%s;port=%s;dbname=%s',
        'localhost',
        '3306',
        'tom_pay'
      ),
      'tom_pay',
      'tom_pay'
    );
    # 抓取表格 pay 的該日期資料，顯示於此
    $sql = "SELECT id, item, dollars FROM pays WHERE date = :date ORDER BY id";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(':date', $date);
    $statement->execute();

    while( ($row = $statement->fetch(PDO::FETCH_ASSOC)) !== false) {
      $list[] = $row;
    }

    if(empty($list)) return [];

    return $list;
  } catch (PDOException $e) {
    echo 'Database connection failed';
    echo $e->getMessage();
    echo $e->getFile();
    echo $e->getLine();
    exit;
  }
}
function create($pay) {
  try {
  # 新增資料到資料庫
  $pdo = new PDO(
           sprintf('mysql:host=%s;port=%s;dbname=%s',
               'localhost',
               '3306',
               'tom_pay'
           ),
           'tom_pay',
           'tom_pay'
  );

  $pdo->beginTransaction();

  $sql = "INSERT INTO pays (date, item, dollars)
    VALUES (:date, :item, :dollars)";
  $statement = $pdo->prepare($sql);

  $statement->bindValue(':date', $pay['date']);
  $statement->bindValue(':item', $pay['item']);
  $statement->bindValue(':dollars', $pay['dollars']);

  $statement->execute();

  $pdo->commit();

  // 成功執行，回傳 true
  return true;
  } catch (PDOException $e) {
    echo 'Database connection failed';
    echo $e->getMessage();
    echo $e->getFile();
    echo $e->getLine();
    return false;
  }
}
function remove($id) {
  try {
    $pdo = new PDO(
             sprintf('mysql:host=%s;port=%s;dbname=%s',
                 'localhost',
                 '3306',
                 'tom_pay'
             ),
             'tom_pay',
             'tom_pay'
    );

    $pdo->beginTransaction();

    $sql = "DELETE FROM pays WHERE id = :id";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(':id', $id, PDO::PARAM_INT);

    $statement->execute();

    $pdo->commit();

    return true;
  } catch (PDOException $e) {
    echo 'Database connection failed';
    echo $e->getMessage();
    echo $e->getFile();
    echo $e->getLine();
    return false;
  }
}
function getWeekLabels() {
  return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
}
# 找到這個月 1 日是星期幾 (回傳 Mon)
function getWeekDayOfFirstDayThisMonth() {
  return date("D", mktime(0, 0, 0, getMonth(), 1, getYear()));
}

function getToday() {
  $d = new DateTime();
  return $d->format('Y-m-d');
}
function get_date() {
  return (isset($_GET['date'])) ? $_GET['date'] : this_year_and_month();
}
function getYear() {
  # 從字串2014-12中抓取2014
  return substr(get_date(), 0, 4);
}
function getMonth() {
  # 從字串2014-12中抓取12
  # 如果月份是 01，則回傳 1
  return sprintf("%d", substr(get_date(), 5));
}
function this_year_and_month() {
  # 回傳這個月的年和月
  date_default_timezone_set('Asia/Taipei');
  return date('Y-m');
}
function DateFormat($year, $month, $day) {
  # 統一日期格式為2014-01-01
  return $year . '-' . sprintf("%02d", $month) . '-' . sprintf("%02d", $day);
}
function number_of_days_last_month() {
  # 上個月有幾天
  date_default_timezone_set('Asia/Taipei');
  return date("t", mktime(0, 0, 0, getMonth() - 1, 1, getYear()));
}
function year_last_month() {
  # 傳回指定年月上個月的年份
  # 如果指定的月是一月，則傳回指定年的上一年，否則傳回指定年
  return (getMonth() == '1') ? getYear() - 1 : getYear();
}
function last_month() {
  # 傳回指定月上個月的月份
  # 如果指定月是一月，則回傳12
  # 否則回傳上一個月
  return (getMonth() == '1') ? 12 : getMonth() -1;
}
function number_of_days_this_month() {
  # 這個月有幾天
  date_default_timezone_set('Asia/Taipei');
  return date("t", mktime(0, 0, 0, getMonth(), 1, getYear()));
}
function next_year() {
  # 傳回指定年月的下一年
  # 如果指定月是12，則回傳下一年
  # 否則回傳同一年
  return (getMonth() == '12') ? getYear() + 1 : getYear();
}
function next_month() {
  # 傳回指定月的下一個月
  # 如果指定月是12，則回傳1
  # 否則回傳下個月
  return (getMonth() == '12') ? 1 : getMonth() + 1;
}
function get_date_and_dollars_this_month($date_from, $date_to) {
  try {
  $pdo = new PDO(
           sprintf('mysql:host=%s;port=%s;dbname=%s',
               'localhost',
               '3306',
               'tom_pay'
           ),
           'tom_pay',
           'tom_pay'
  );

  $sql = "SELECT date, dollars FROM pays
    WHERE date BETWEEN :date_from AND :date_to
    ORDER BY date ASC, id ASC";

$statement = $pdo->prepare($sql);
$statement->bindValue(':date_from', $date_from);
$statement->bindValue(':date_to', $date_to);
$statement->execute();

  if( $statement->rowCount() === 0)
    return [];

  while( ($row = $statement->fetch(PDO::FETCH_ASSOC)) !== false) {
    $data[] = $row;
  }
  return $data;
} catch (PDOException $e) {
echo 'Database connection failed';
echo $e->getMessage();
echo $e->getFile();
echo $e->getLine();
exit;
}
}
function calc_sum_each_day($data) {
  $current_date = '1900-01-01';
  $sum = array();
  # 如果下一列是新的日期，則更新日期，小計重新計算
  for($i = 0; $i < count($data); $i++) {
    $next_date = $data[$i]['date'];
    $dollars   = $data[$i]['dollars'];
    # 如果下一列是新的日期
    if($next_date != $current_date) {
      # 更新日期
      $current_date = $next_date;
      # 小計重新計算
      $sum[$current_date] = $dollars;
    }
    # 如果是相同日期
    else {
      # 加總支出金額
      $sum[$current_date] += $dollars;
    }
  }
  return $sum;
}
function combine_sum_with_calendar($data, $calendar) {
  foreach($data as $date => $sum) {
    # 依照小計的日期，尋找 $calendar 內相同的日期
    # 找到的話，把小計放到欄位 ['sum']
    for($row = 1; $row < count($calendar); $row++) {
      for($col = 0; $col < count($calendar[$row]); $col++) {
        if($date == $calendar[$row][$col]['date']) {
	      $calendar[$row][$col]['sum'] += $sum;
        }
      }
    }
  }
  return $calendar;
}
function total_of_month() {
  # 計算這個月支出總計
  #   抓取這個月有幾天
  #   計算支出總計，儲存到變數 $total_of_month
  $date_from = DateFormat(getYear(), getMonth(), 1);
  $date_to = DateFormat(getYear(), getMonth(), number_of_days_this_month());

  $date_and_dollars_this_month = get_date_and_dollars_this_month($date_from, $date_to);

  $total = 0;
  for($i = 0; $i < count($date_and_dollars_this_month); $i++) {
    $total += $date_and_dollars_this_month[$i]['dollars'];
  }
  return $total;
}

####### view 相關 function #######

function showCalendarTitle() {
  return getFullMonth(getMonth()) . ' ' . getYear();
}
function lastMonthYear() {
  $year = getYear();
  $month = getMonth();
  # 抓取上個月的年份和月份
  $year_last_month = year_last_month($year, $month);
  $last_month = last_month();
  # 回傳年和月
  return $year_last_month . '-' . sprintf("%02d", $last_month);
}
function lastMonthYearTitle() {
  $year = getYear();
  $month = getMonth();
  # 抓取上個月的年份和月份
  $year_last_month = year_last_month($year, $month);
  $last_month = last_month();
  # 抓取December
  $lastFullMonth = getFullMonth($last_month);
  # 如果月份是12，表示上個月是不同的年份，顯示年和月
  # 否則只顯示月
  if($last_month == 12)
    return "$lastFullMonth $year_last_month";
  else
    return $lastFullMonth;
}
function nextMonthYear() {
  $year = getYear();
  $month = getMonth();
  # 抓取下個月的年份和月份
  $year_next_month = next_year();
  $next_month = next_month();
  # 回傳年和月
  return $year_next_month . '-' . sprintf("%02d", $next_month);
}
function nextMonthYearTitle() {
  $year = getYear();
  $month = getMonth();
  # 抓取下個月的年份和月份
  $year_next_month = next_year();
  $next_month = next_month();
  # 抓取December
  $nextFullMonth = getFullMonth($next_month);
  # 如果月份是1，表示下個月是不同的年份，顯示年和月
  # 否則只顯示月
  if($next_month == 1)
    return "$nextFullMonth $year_next_month";
  else
    return $nextFullMonth;
}
function getFullMonth($month) {
  date_default_timezone_set('Asia/Taipei');
  return date("F", mktime(0, 0, 0, $month, 1, 2014));
}
function showOnlyDay($date) {
  # 2014-12-30 只顯示 30
  return sprintf("%d", substr($date, 8, 2));
}
function showSum($sum) {
  # 第一列的星期沒有欄位 ['sum']
  # &nbsp; 是HTML的空白
  return (isset($sum) && $sum != 0) ? $sum : '&nbsp;';
}
function getCalendar() {
  ############## 產生月曆所需的陣列 $calendar ##################

  # 參考 http://still-wildwood-6352.herokuapp.com/issues/410
  # 產生星期標示
  # 1. 新增一列（7欄），填上星期
  $calendar = [getWeekLabels()];

  date_default_timezone_set('Asia/Taipei');

  # 2. 找到這個月 1 日是星期一（Mon），則先把 1 放到第 2 欄（Mon所在欄）
  # 2.1. 這個月的 1 日是星期幾
  # 2.2. 如果是星期一，則 Mon 在 getWeekLabels() 的索引編號
  $index_of_array_day = array_search(getWeekDayOfFirstDayThisMonth(), getWeekLabels());
  $calendar[1][$index_of_array_day] = [
    'date' => DateFormat(getYear(), getMonth(), 1),
    'sum' => 0];

  # 3. 如果 1 日不是在第 1 欄，檢查之前有幾欄
  #    則用上個月倒數第 1 天、倒數第 2 天...填滿
  if ($index_of_array_day != 0) {
    # 索引值為 1，則前面還有 1 欄，索引值為 2，則還有 2 欄

    for($i = 0; $i < $index_of_array_day; $i++) {
      $fill_day        = number_of_days_last_month() - $index_of_array_day + 1 + $i;
      $calendar[1][$i] = [
        'date' => DateFormat(year_last_month(), last_month(), $fill_day),
        'sum' => 0];
    }
  }

  # 4. 在 1 日的下一欄開始，填上 2, 3, 4, 5, 6
  #    直到 1 日之後的欄位都填滿了
  # getWeekLabels() 總共有 7 欄，減掉 $calendar 第二列已有的元素個數
  # 即為 1 日之後有多少欄
  $number_of_columns_after_1 = count(getWeekLabels()) - count($calendar[1]);

  for($i = 0; $i < $number_of_columns_after_1; $i++) {
    $d = $i + 2;
    # 從 1 日的下一個開始填入
    $calendar[1][$index_of_array_day + $i + 1] = [
      'date' => DateFormat(getYear(), getMonth(), $d),
      'sum' => 0];
  }

  # 如果 1 日之後的欄位數量為 0，則上面的 for 迴圈不會執行
  # 表示直接新增下一列，從 2 日開始填入
  if($number_of_columns_after_1 == 0) $d = 1; # while 迴圈內會先遞增到 2

  # 5. 新增一列，填入 7, 8, 9...，直到填滿所有欄
  # 6. 重複第 5 項，直到填入這個月的最後一天
  # 這個月有幾天
  $row = 2;
  while( $d < number_of_days_this_month()) {
    for($i = 0; $i < count(getWeekLabels()); $i++) {
      $d++;
      $calendar[$row][$i] = [
        'date' => DateFormat(getYear(), getMonth(), $d),
        'sum' => 0];
      if($d == number_of_days_this_month()) break;
    }
    $row++; # 新增一列
  }

  # 7. 如果這個月的最後一天不是填在第 7 欄，檢查還有多少欄
  #    則從 1 開始填入下一欄，直到欄位都填滿了
  $number_of_columns_lastest_row = count($calendar[$row - 1]);
  if($number_of_columns_lastest_row != count(getWeekLabels())) {
    $still_columns = count(getWeekLabels()) - $number_of_columns_lastest_row;

    for($i = 0; $i < $still_columns; $i++) {
      $d = $i + 1;
      $calendar[$row - 1][$number_of_columns_lastest_row + $i] = [
        'date' => DateFormat(next_year(), next_month(), $d),
        'sum' => 0];
    }
  }
  ################ 月曆所需陣列 $calendar 到此結束 ####################

  # 每個日期下方顯示該日期小計
  # 抓取該月份所有支出記錄的日期和金額，依照日期遞增、id遞增來排列
  #   $calendar[1][0]['date']：左上方第一個日期
  #   $calendar[count($calendar)-1][5]['date']：右下方最後一個日期
  $date_and_dollars_this_month = get_date_and_dollars_this_month($calendar[1][0]['date'], $calendar[count($calendar)-1][5]['date']);
  # 計算每個日期的小計，儲存到 $sum[日期] = 小計
  $sum = calc_sum_each_day($date_and_dollars_this_month);
  # 合併 $sum 到 $calendar，變成 $calendar[列][欄]['sum'] = 小計
  $calendar = combine_sum_with_calendar($sum, $calendar);

  return $calendar;
}
# 計算每星期小計
function getWeekSum() {
  $calendar = getCalendar();

  $week_sum = [
    ['sum' => 'total of week']
  ];

  for($row = 1; $row < count($calendar); $row++) {
    $week_sum[$row] = ['sum' => 0];
    for($col = 0; $col < count($calendar[$row]); $col++) {
      $week_sum[$row]['sum'] += $calendar[$row][$col]['sum'];
    }
  }

  return $week_sum;
}
